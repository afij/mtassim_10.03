/* 
 * File:   MTASLeadBlanket.hh
 * Author: aleksandra
 *
 * Lead blankets
 */
#ifndef MTASLeadBlanket_h
#define MTASLeadBlanket_h 1
#include "MTASMaterialsAndColorsManager.hh"
#include "G4LogicalVolume.hh"
#include "globals.hh"

class MTASLeadBlanket
{
public:
	MTASLeadBlanket(G4LogicalVolume* );
	~MTASLeadBlanket();
	
private:
	void SetParameters();
	void Construct();
	void SetVisibleAtributes();

	G4LogicalVolume* m_logicWorld;
	G4LogicalVolume* leadBlanketLogic;

	MTASMaterialsAndColorsManager* m_materialsAndColorsManager;
	
	G4double m_inch; 
	
	G4double leadInternalSide;
    G4double leadLength;
    G4double leadThickness;
    G4double centralHoleRadius;
};
	
#endif
