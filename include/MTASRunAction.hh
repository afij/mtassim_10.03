//
// $Id: MTASRunAction.hh 23.04.2018 A Fijalkowska $
// 
/// \file MTASRunAction.hh
/// \brief Definition of the MTASRunAction class

#ifndef MTASRunAction_h
#define MTASRunAction_h 1

#include "G4UserRunAction.hh"
#include "G4String.hh"
#include "G4Run.hh"

class MTASRunAction : public G4UserRunAction
{
  public:
    MTASRunAction();
    virtual ~MTASRunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void   EndOfRunAction(const G4Run*);
  private:
    G4String MakeOutputFileName();
    G4int totNrOfEvents;
};


#endif
