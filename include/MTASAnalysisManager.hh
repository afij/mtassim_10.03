

#ifndef MTASAnalysisManger_h
#define MTASAnalysisManger_h 1

#include <vector>
#include "G4THitsMap.hh"
#include "G4RootAnalysisManager.hh"
#include "G4String.hh"

class MTASAnalysisManager
{
    public:
	   MTASAnalysisManager();
	//virtual ~MTASAnalysisManager();

		void CreateOutput(G4String filename);
		void SaveOutput();
		void AddEnergytHit(double energyCentralDet, G4THitsMap<G4double>* energyOutersDets, 
                                        double maxBetaVal, int maxBetaId, int eventId);
		void AddLightHit(std::vector <double>& light, double maxBetaVal, int maxBetaId, int eventId);
		void AddBetaHit(G4THitsMap<G4double>* betaDets, int eventId);
		
	private:
		void CreateTuple();
		void CreateLightBranch();
		void CreateEnergyBranch();
		void CreateBetaEnergyBranch();
		
		static MTASAnalysisManager *s_instance;
		G4RootAnalysisManager* rootManager;
		G4int nrOfCreatedBranches;		
		G4int lightBranchId;
		G4int energyDepBranchId;
		G4int betaEnergyBranchId;
		
		
	public:
	static MTASAnalysisManager* GetInstance()
	{
		if (!s_instance)
			s_instance = new MTASAnalysisManager();
		return s_instance;
	}

};


#endif // MTASAnalysisManger_h

