//
// $Id: MTASRunAction.cc 12.10.2016 A Fijalkowska $
//
/// \file MTASRunAction.cc
/// \brief Implementation of the MTASRunAction class

#include "MTASRunAction.hh"
#include "MTASAnalysisManager.hh"



MTASRunAction::MTASRunAction(): G4UserRunAction()
{ 

   totNrOfEvents = 0;
   MTASAnalysisManager* analysisManager = MTASAnalysisManager::GetInstance();
   G4String fileName = MakeOutputFileName();
   std::cout<<fileName<<std::endl; 
   analysisManager->CreateOutput(fileName);
}


MTASRunAction::~MTASRunAction()
{
   MTASAnalysisManager* analysisManager = MTASAnalysisManager::GetInstance();
   analysisManager->SaveOutput();  
}

void MTASRunAction::BeginOfRunAction(const G4Run* run)
{ 
   G4cout << "### Run " << run->GetRunID() << " start." << G4endl; 	

}


void MTASRunAction::EndOfRunAction(const G4Run* run)
{
   G4cout << "number of event = " << run->GetNumberOfEvent() << G4endl;
   totNrOfEvents += run->GetNumberOfEvent();   

}


G4String MTASRunAction::MakeOutputFileName()
{
	return "./MTASOutput.root";
}

