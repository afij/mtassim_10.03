/**
* Class constructing lead blankets (as a one layer) 
*/

#include <math.h> 

#include "MTASLeadBlanket.hh"
#include "G4Material.hh"
#include "G4SubtractionSolid.hh"
#include "G4Polyhedra.hh"
#include "G4Tubs.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

  
MTASLeadBlanket::MTASLeadBlanket(G4LogicalVolume* logicWorld) 
{
	m_logicWorld=logicWorld;
	m_materialsAndColorsManager = MTASMaterialsAndColorsManager::GetInstance();
	SetParameters();
	Construct();
	SetVisibleAtributes();
}
 
MTASLeadBlanket::~MTASLeadBlanket()
{
	//if(m_materialsAndColorsManager != 0)
		//delete m_materialsAndColorsManager;
}

void MTASLeadBlanket::SetParameters()
{ 
	m_inch=2.54*cm; 		
	G4double cristalSide=4*m_inch;
	leadInternalSide = 5.* cristalSide + 5*cm;
    leadLength = 1.35*m;
    leadThickness = 3*cm;
    centralHoleRadius = 4 *m_inch;

}  
  
  
void MTASLeadBlanket::Construct()
{

	G4Material* blanketMaterial = m_materialsAndColorsManager->GetLead();

	const G4int numberSides = 6;
	const G4int numberZPlanes = 2;
	const G4double  startPhi = 30.0  * degree;
	const G4double  deltaPhi = 390.0 * degree;
	const G4double rZero[numberZPlanes] = { 0.0, 0.0 };

//thick plate	
	const G4double radiusOut[2] = { leadInternalSide, leadInternalSide };
	const G4double zPlanesOut[2] = { -leadLength/2., leadLength/2. };
	G4Polyhedra* blanketSolid1 = new G4Polyhedra("blanketSolid1", 
	                                             startPhi,
	                                             deltaPhi,  
	                                             numberSides, 
	                                             numberZPlanes, 
	                                             zPlanesOut, 
	                                             rZero, 
	                                             radiusOut );

	                                             
	const G4double radiusIn[2] = {leadInternalSide - leadThickness, 
		                          leadInternalSide - leadThickness};
	const G4double zPlanesIn[2] = {-(leadLength-leadThickness)/2., 
		                            (leadLength-leadThickness)/2.};  
		                                                                        
	G4Polyhedra* blanketSolid2 = new G4Polyhedra("blanketSolid2", 
	                                             startPhi,
	                                             deltaPhi,  
	                                             numberSides, 
	                                             numberZPlanes, 
	                                             zPlanesIn, 
	                                             rZero, 
	                                             radiusIn );
	 
	 
	 
	//G4RotationMatrix *zRot = new G4RotationMatrix;
	//zRot -> rotateZ(0*rad);
	G4ThreeVector zTrans(0.0,0.0,0.0);
	G4SubtractionSolid* blanketSolid12 = new G4SubtractionSolid("blanketSolid12",
	                                                          blanketSolid1, 
	                                                          blanketSolid2, 
	                                                          0, 
	                                                          zTrans);
	                                                          
	//central hole
	G4Tubs* centralHoleSolid = new G4Tubs ("centralHoleSolid", 
	                                       0, 
	                                       centralHoleRadius, 
	                                       (leadLength+leadThickness)/2., 
	                                       startPhi, 
	                                       deltaPhi);
	                                       
	G4SubtractionSolid* blanketSolid = new G4SubtractionSolid("blanketSolid",
	                                                          blanketSolid12, 
	                                                          centralHoleSolid, 
	                                                          0, 
	                                                          zTrans);
    
	leadBlanketLogic = new G4LogicalVolume(blanketSolid, 
	                                       blanketMaterial, 
	                                       "leadBlanketLogic");  

	G4ThreeVector position( 0., 0., 0. );
	new G4PVPlacement( 0, position, leadBlanketLogic, "leadBlanket", m_logicWorld, 0, 0 );


}


void MTASLeadBlanket::SetVisibleAtributes()
{
	leadBlanketLogic->SetVisAttributes(m_materialsAndColorsManager->GetRed());
  
}

