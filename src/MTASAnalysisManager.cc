#include "MTASAnalysisManager.hh"
#include "G4SystemOfUnits.hh"
#include <iostream>


MTASAnalysisManager::MTASAnalysisManager()
{
    rootManager = G4RootAnalysisManager::Instance();	
    rootManager->SetVerboseLevel(1);
    rootManager->SetFirstHistoId(0);
    rootManager->SetFirstNtupleId(0);
    rootManager->SetFirstNtupleColumnId(0);  
    nrOfCreatedBranches = 0;

}

void MTASAnalysisManager::CreateOutput(G4String filename)
{
    rootManager->OpenFile(filename);
    CreateTuple();   
}


void MTASAnalysisManager::SaveOutput()
{
    rootManager->Write();
    rootManager->CloseFile();	
}

void MTASAnalysisManager::CreateTuple()
{
    CreateLightBranch();
    CreateEnergyBranch();
    CreateBetaEnergyBranch();
}

void MTASAnalysisManager::CreateLightBranch()
{
   rootManager->CreateNtuple("Light", "Light emit in MTAS");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleIColumn("betaId");
   rootManager->CreateNtupleDColumn("maxBeta");  
   rootManager->CreateNtupleDColumn("C");
   rootManager->CreateNtupleDColumn("I1");
   rootManager->CreateNtupleDColumn("I2");
   rootManager->CreateNtupleDColumn("I3");
   rootManager->CreateNtupleDColumn("I4");
   rootManager->CreateNtupleDColumn("I5");
   rootManager->CreateNtupleDColumn("I6");
   rootManager->CreateNtupleDColumn("M1");
   rootManager->CreateNtupleDColumn("M2");
   rootManager->CreateNtupleDColumn("M3");
   rootManager->CreateNtupleDColumn("M4");
   rootManager->CreateNtupleDColumn("M5");
   rootManager->CreateNtupleDColumn("M6");
   rootManager->CreateNtupleDColumn("O1");
   rootManager->CreateNtupleDColumn("O2");
   rootManager->CreateNtupleDColumn("O3");
   rootManager->CreateNtupleDColumn("O4");
   rootManager->CreateNtupleDColumn("O5");
   rootManager->CreateNtupleDColumn("O6");
   rootManager->FinishNtuple();
   lightBranchId = nrOfCreatedBranches++;
   
   int colId = 0;
   rootManager->FillNtupleIColumn(lightBranchId, colId, 0); 
   rootManager->FillNtupleIColumn(lightBranchId, ++colId, 0);
   for(int i = 0;  i != 20; ++i)
   {
	   rootManager->FillNtupleDColumn(lightBranchId, ++colId, 0.);
   }		   
   rootManager->AddNtupleRow(lightBranchId);     
}



void MTASAnalysisManager::CreateEnergyBranch()
{
   rootManager->CreateNtuple("EnergyDep", "Energy dep in MTAS");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleIColumn("betaId");
   rootManager->CreateNtupleDColumn("maxBeta");  
   rootManager->CreateNtupleDColumn("C");
   rootManager->CreateNtupleDColumn("I1");
   rootManager->CreateNtupleDColumn("I2");
   rootManager->CreateNtupleDColumn("I3");
   rootManager->CreateNtupleDColumn("I4");
   rootManager->CreateNtupleDColumn("I5");
   rootManager->CreateNtupleDColumn("I6");
   rootManager->CreateNtupleDColumn("M1");
   rootManager->CreateNtupleDColumn("M2");
   rootManager->CreateNtupleDColumn("M3");
   rootManager->CreateNtupleDColumn("M4");
   rootManager->CreateNtupleDColumn("M5");
   rootManager->CreateNtupleDColumn("M6");
   rootManager->CreateNtupleDColumn("O1");
   rootManager->CreateNtupleDColumn("O2");
   rootManager->CreateNtupleDColumn("O3");
   rootManager->CreateNtupleDColumn("O4");
   rootManager->CreateNtupleDColumn("O5");
   rootManager->CreateNtupleDColumn("O6");
   rootManager->FinishNtuple();
   energyDepBranchId = nrOfCreatedBranches++;
   
   int colId = 0;
   rootManager->FillNtupleIColumn(energyDepBranchId, colId, 0); 
   rootManager->FillNtupleIColumn(energyDepBranchId, ++colId, 0);
   for(int i = 0;  i != 20; ++i)
   {
	   rootManager->FillNtupleDColumn(energyDepBranchId, ++colId, 0.);
   }		   
   rootManager->AddNtupleRow(energyDepBranchId);     
}

void MTASAnalysisManager::CreateBetaEnergyBranch()
{
   rootManager->CreateNtuple("BetaEnergyDep", "Energy dep in silicond");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleDColumn("B1top");
   rootManager->CreateNtupleDColumn("B2top");
   rootManager->CreateNtupleDColumn("B3top");
   rootManager->CreateNtupleDColumn("B4top");
   rootManager->CreateNtupleDColumn("B5top");
   rootManager->CreateNtupleDColumn("B6top");
   rootManager->CreateNtupleDColumn("B7top");
   rootManager->CreateNtupleDColumn("B1bot");
   rootManager->CreateNtupleDColumn("B2bot");
   rootManager->CreateNtupleDColumn("B3tbot");
   rootManager->CreateNtupleDColumn("B4bot");
   rootManager->CreateNtupleDColumn("B5bot");
   rootManager->CreateNtupleDColumn("B6bot");
   rootManager->CreateNtupleDColumn("B7bot");

   rootManager->FinishNtuple();
   betaEnergyBranchId = nrOfCreatedBranches++;
   
   int colId = 0;
   rootManager->FillNtupleIColumn(betaEnergyBranchId, colId, 0); 
   for(int i = 0;  i != 14; ++i)
   {
	   rootManager->FillNtupleDColumn(betaEnergyBranchId, ++colId, 0.);
   }		   
   rootManager->AddNtupleRow(betaEnergyBranchId);     
}



void MTASAnalysisManager::AddEnergytHit(double energyCentralDet, G4THitsMap<G4double>* energyOutersDets, 
                                        double maxBetaVal, int maxBetaId, int eventId)
{
    int colId = 0;
    rootManager->FillNtupleIColumn(energyDepBranchId, colId, eventId); 
    rootManager->FillNtupleIColumn(energyDepBranchId, ++colId, maxBetaId); 
    rootManager->FillNtupleDColumn(energyDepBranchId, ++colId, maxBetaVal/keV); 
    rootManager->FillNtupleDColumn(energyDepBranchId, ++colId, energyCentralDet/keV);   
    
    for(int i=0;i<18;i++)
	{ 
		double energyDep = 0;
		if((*energyOutersDets)[i]!=0)
		{
			energyDep=*((*energyOutersDets)[i]);
		}
		rootManager->FillNtupleDColumn(energyDepBranchId, ++colId, energyDep/keV);
	} 
	rootManager->AddNtupleRow(energyDepBranchId);   
}


void MTASAnalysisManager::AddLightHit(std::vector <double>& light, double maxBetaVal, int maxBetaId, int eventId)
{
    int colId = 0;
    rootManager->FillNtupleIColumn(lightBranchId, colId, eventId); 
    rootManager->FillNtupleIColumn(lightBranchId, ++colId, maxBetaId); 
    rootManager->FillNtupleDColumn(lightBranchId, ++colId, maxBetaVal/keV);    
    
    for(int i=0;i<19;i++)
	{ 
		rootManager->FillNtupleDColumn(lightBranchId, ++colId, light.at(i));		
	}  
	rootManager->AddNtupleRow(lightBranchId); 
}


void MTASAnalysisManager::AddBetaHit(G4THitsMap<G4double>* betaDets, int eventId)
{
    int colId = 0;
    rootManager->FillNtupleIColumn(betaEnergyBranchId, colId, eventId);  
    
    for(int i=0;i<14;i++)
	{ 
		double betaEnDep = 0;
		if((*betaDets)[i]!=0)
		{
			betaEnDep=*((*betaDets)[i]);
		}
		rootManager->FillNtupleDColumn(betaEnergyBranchId, ++colId, betaEnDep/keV);
	}
	rootManager->AddNtupleRow(betaEnergyBranchId);  
}

MTASAnalysisManager *MTASAnalysisManager::s_instance = 0;
