
#include "MTASDetectorConstruction.hh"
#include "G4HCofThisEvent.hh"

#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4ios.hh"
#include "G4SDManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4VHit.hh"
#include "G4SystemOfUnits.hh" 
#include "MTASEventAction.hh"
#include "MTASPrimaryGeneratorAction.hh"
#include "MTASSteppingAction.hh"
#include <stdio.h>
#include <string>
MTASEventAction::MTASEventAction()
{
	StartUp();
}
 
MTASEventAction::~MTASEventAction()
{
}
 
void MTASEventAction::StartUp()
{
	analysisManager = MTASAnalysisManager::GetInstance();
	G4SDManager* SDman = G4SDManager::GetSDMpointer();
  
	G4String Name1="NaIOuter/eDep";
	G4String Name2="NaICenter/eDep";
	G4String Name3="Sili/eDep";
  
	colIDSumCenter = SDman->GetCollectionID(Name2);
	colIDSumOuter = SDman->GetCollectionID(Name1);
	colIDSumSili = SDman->GetCollectionID(Name3);
}

void MTASEventAction::BeginOfEventAction(const G4Event* )
{
	
}

 
void MTASEventAction::EndOfEventAction(const G4Event* evt)
{
	G4int eventId = evt->GetEventID();
	G4HCofThisEvent* HCE = evt->GetHCofThisEvent();
	if(!HCE) return;
	
//sensitives detectors
	G4THitsMap<G4double>* evtMapOuter = (G4THitsMap<G4double>*)(HCE->GetHC(colIDSumOuter));
	G4THitsMap<G4double>* evtMapCenter = (G4THitsMap<G4double>*)(HCE->GetHC(colIDSumCenter));
	G4THitsMap<G4double>* evtMapSili = (G4THitsMap<G4double>*)(HCE->GetHC(colIDSumSili));

    int betaDetId;
    double maxBetaEn = FindMaxSiliEnergyDep(evtMapSili, betaDetId);
    double centralDetEn = 0.;
    if((*evtMapCenter)[0] != 0)
		centralDetEn = *((*evtMapCenter)[0]);

    analysisManager->AddEnergytHit(centralDetEn, evtMapOuter,maxBetaEn, betaDetId, eventId);
	analysisManager->AddLightHit(MTASSteppingAction::lightProdInNaIMod, maxBetaEn, betaDetId, eventId);
	analysisManager->AddBetaHit(evtMapSili, eventId);

  	for(int i=0; i<19; i++)
  		MTASSteppingAction::lightProdInNaIMod[i] = 0;	
  	
	
// THIS IS FOR GEANT OUTPUT AND VISUALIZATION OUT, NO HISTOGRAMS INFO DOWN HERE
// In order for this to work you must set the "/tracking/storeTrajectory 1" in the macro. 
// For us this is turned on in the vis.mac
// get number of stored trajectories
	G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
	G4int n_trajectories = 0;
	if( trajectoryContainer ) n_trajectories = trajectoryContainer->entries();
  

// periodic printing every 100 events
	if( eventId % 100 == 0 )
	{
		G4cout << "Finished Running Event # " << eventId << G4endl;
	}
    
// extract the trajectories and draw them
	if( G4VVisManager::GetConcreteInstance() )
	{
	   for( G4int i=0; i < n_trajectories; i++ ) 
		{ 
			G4Trajectory* trj = (G4Trajectory*)( (*( evt->GetTrajectoryContainer() ))[i] );
			trj->DrawTrajectory();
		}
	}
	
}


double MTASEventAction::FindMaxSiliEnergyDep(G4THitsMap<G4double>* evtMapSili, int& detectorId)
{
	int maxEnDetId = 0;
	double maxEnergy = 0;
    for(int i=0;i<14;i++)
	{ 	
	    if((*evtMapSili)[i] !=0)
		{
			if( *((*evtMapSili)[i]) > maxEnergy)
			{
			    maxEnergy = *((*evtMapSili)[i]);
			    maxEnDetId = i;   
			}
		}
    } 	
	
	detectorId = maxEnDetId;
	return maxEnergy;
}
